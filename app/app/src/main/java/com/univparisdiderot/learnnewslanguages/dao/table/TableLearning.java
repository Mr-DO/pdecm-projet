package com.univparisdiderot.learnnewslanguages.dao.table;

public class TableLearning implements Table {

    public enum Level{
        LOW,
        MEDIUM,
        HIGH
    }

    private static final String name = "languages";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_USER = "user";
    public static final String COLUMN_WORD = "word";
    public static final String COLUMN_LEVEL_TRUE = "answerTrue";
    public static final String COLUMN_LEVEL_FALSE = "answerFalse";

    private static final String descriptionTable = "CREATE TABLE " + name + " (" +
        COLUMN_ID + " integer PRIMARY KEY AUTOINCREMENT, " +
        COLUMN_USER + " integer NOT NULL, " +
        COLUMN_WORD + " integer NOT NULL, " +
        COLUMN_LEVEL_TRUE + " integer DEFAULT 0, " +
        COLUMN_LEVEL_FALSE + " integer DEFAULT 0, " +
        "FOREIGN KEY (" + COLUMN_USER + ") REFERENCES " + TableUsers.name + " (" + TableUsers.COLUMN_ID + "), " +
        "FOREIGN KEY (" + COLUMN_WORD + ") REFERENCES " + TableWords.name + " (" + TableWords.COLUMN_ID + ") " +
    ")";

    private static TableLearning instance = null;

    public static TableLearning getInstance(){
        if(instance == null){
            instance = new TableLearning();
        }
        return instance;
    }

    @Override
    public String nameOfTable() {
        return name;
    }

    @Override
    public String descriptionToCreateTable() {
        return descriptionTable;
    }

    public String getLevelText(Level level){
        switch (level){
            case MEDIUM:
                return "medium";
            case HIGH:
                return "high";
            default:
                return "low";
        }
    }
}
