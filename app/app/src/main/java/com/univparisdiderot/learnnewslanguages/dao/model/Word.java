package com.univparisdiderot.learnnewslanguages.dao.model;

import android.content.ContentValues;

import com.univparisdiderot.learnnewslanguages.dao.table.TableWords;

public class Word implements Model {

    private ContentValues contentValues;

    private int id;
    private int language;
    private String word;
    private String localeImage;
    private String internetImage;
    private String vocal;

    public Word(int id, int language, String word, String localeImage, String internetImage, String vocal){
        this.id = id;
        this.language = language;
        this.word = word;
        this.localeImage = localeImage;
        this.internetImage = internetImage;
        this.vocal = vocal;
    }

    //getters
    public int getId() {
        return id;
    }
    public int getLanguage() {
        return language;
    }
    public String getWord() {
        return word;
    }
    public String getLocaleImage() {
        return localeImage;
    }
    public String getInternetImage() {
        return internetImage;
    }
    public String getVocal() {
        return vocal;
    }

    // setters
    public void setId(int id) {
        this.id = id;
    }
    public void setLanguage(int language) {
        this.language = language;
    }
    public void setWord(String word) {
        this.word = word;
    }
    public void setLocaleImage(String localeImage) {
        this.localeImage = localeImage;
    }
    public void setInternetImage(String internetImage) {
        this.internetImage = internetImage;
    }
    public void setVocal(String vocal) {
        this.vocal = vocal;
    }

    @Override
    public ContentValues getContentValues() {
        this.contentValues.put(TableWords.COLUMN_LANGUAGE, this.language);
        this.contentValues.put(TableWords.COLUMN_WORD, this.word);
        this.contentValues.put(TableWords.COLUMN_LOCALE_IMAGE, this.localeImage);
        this.contentValues.put(TableWords.COLUMN_INTERNET_IMAGE, this.internetImage);
        this.contentValues.put(TableWords.COLUMN_VOCAL, this.vocal);
        return this.contentValues;
    }
}
