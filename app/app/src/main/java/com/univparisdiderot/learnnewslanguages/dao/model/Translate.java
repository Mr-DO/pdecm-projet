package com.univparisdiderot.learnnewslanguages.dao.model;

import android.content.ContentValues;

import com.univparisdiderot.learnnewslanguages.dao.table.TableTranslate;

public class Translate implements Model {

    private ContentValues contentValues;

    private int id;
    private int word;
    private int wordTranslate;

    public Translate(int id, int word, int wordTranslate){
        this.id = id;
        this.word = word;
        this.wordTranslate = wordTranslate;
    }

    // getters
    public int getId() {
        return id;
    }
    public int getWord() {
        return word;
    }
    public int getWordTranslate() {
        return wordTranslate;
    }

    // setters
    public void setId(int id) {
        this.id = id;
    }
    public void setWord(int word) {
        this.word = word;
    }
    public void setWordTranslate(int wordTranslate) {
        this.wordTranslate = wordTranslate;
    }

    @Override
    public ContentValues getContentValues() {
        this.contentValues.put(TableTranslate.COLUMN_WORD, this.word);
        this.contentValues.put(TableTranslate.COLUMN_WORD_TRANSLATE, wordTranslate);
        return this.contentValues;
    }
}
