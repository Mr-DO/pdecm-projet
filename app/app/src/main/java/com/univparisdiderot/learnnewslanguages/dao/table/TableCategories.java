package com.univparisdiderot.learnnewslanguages.dao.table;

public class TableCategories implements Table {

    public static final String name = "categories";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_DESCRIPTION = "description";

    private static final String descriptionTable = "CREATE TABLE " + name + " (" +
        COLUMN_ID + " integer PRIMARY KEY AUTOINCREMENT, " +
        COLUMN_NAME + " string UNIQUE NOT NULL, " +
        COLUMN_DESCRIPTION + " text" +
    ")";

    private static TableCategories instance = null;

    public static TableCategories getInstance(){
        if(instance == null){
            instance = new TableCategories();
        }
        return instance;
    }

    @Override
    public String nameOfTable() {
        return name;
    }

    @Override
    public String descriptionToCreateTable() {
        return descriptionTable;
    }
}
