package com.univparisdiderot.learnnewslanguages.dao.table;

public class TableLanguages implements Table {

    public static final String name = "languages";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";

    private static final String descriptionTable = "CREATE TABLE " + name + " (" +
        COLUMN_ID + " integer PRIMARY KEY AUTOINCREMENT, " +
        COLUMN_NAME + " string UNIQUE NOT NULL" +
    ")";

    private static TableLanguages instance = null;

    public static TableLanguages getInstance(){
        if(instance == null){
            instance = new TableLanguages();
        }
        return instance;
    }

    @Override
    public String nameOfTable() {
        return name;
    }

    @Override
    public String descriptionToCreateTable() {
        return descriptionTable;
    }
}
