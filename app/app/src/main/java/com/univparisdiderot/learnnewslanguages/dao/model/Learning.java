package com.univparisdiderot.learnnewslanguages.dao.model;

import android.content.ContentValues;

import com.univparisdiderot.learnnewslanguages.dao.table.TableLearning;

public class Learning implements Model {

    private ContentValues contentValues;

    private int id;
    private int user;
    private int word;
    private int answerTrue;
    public int answerFalse;

    public Learning(int id, int user, int word, int answerTrue, int answerFalse){
        this.id = id;
        this.user = user;
        this.word = word;
        this.answerTrue = answerTrue;
        this.answerFalse = answerFalse;
    }

    // getters
    public int getId() {
        return this.id;
    }
    public int getUser() {
        return this.user;
    }
    public int getWord() {
        return this.word;
    }
    public int getAnswerTrue() {
        return this.answerTrue;
    }
    public int getAnswerFalse() {
        return this.answerFalse;
    }

    // setters
    public void setId(int id) {
        this.id = id;
    }
    public void setUser(int user) {
        this.user = user;
    }
    public void setWord(int word) {
        this.word = word;
    }
    public void setAnswerTrue(int answerTrue) {
        this.answerTrue = answerTrue;
    }
    public void setAnswerFalse(int answerFalse) {
        this.answerFalse = answerFalse;
    }

    @Override
    public ContentValues getContentValues() {
        this.contentValues.put(TableLearning.COLUMN_USER, this.user);
        this.contentValues.put(TableLearning.COLUMN_WORD, this.word);
        this.contentValues.put(TableLearning.COLUMN_LEVEL_TRUE, this.answerTrue);
        this.contentValues.put(TableLearning.COLUMN_LEVEL_FALSE, this.answerFalse);
        return this.contentValues;
    }
}
