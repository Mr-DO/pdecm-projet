package com.univparisdiderot.learnnewslanguages.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.univparisdiderot.learnnewslanguages.dao.table.Table;
import com.univparisdiderot.learnnewslanguages.dao.table.TableCategories;
import com.univparisdiderot.learnnewslanguages.dao.table.TableLanguages;
import com.univparisdiderot.learnnewslanguages.dao.table.TableLearning;
import com.univparisdiderot.learnnewslanguages.dao.table.TableTranslate;
import com.univparisdiderot.learnnewslanguages.dao.table.TableUsers;
import com.univparisdiderot.learnnewslanguages.dao.table.TableWords;

public class DataBaseHelper extends SQLiteOpenHelper {


    private final static String NAME_OF_DB = "learn_new_languages";
    private final static int VERSION = 1;

    private Table usersTable = TableUsers.getInstance();
    private Table languagesTable = TableLanguages.getInstance();
    private Table wordsTable = TableWords.getInstance();
    private Table categoriesTable = TableCategories.getInstance();
    private Table learningTable = TableLearning.getInstance();
    private Table translateTable = TableTranslate.getInstance();

    private static DataBaseHelper instanceOfDataBaseHelper = null;

    private DataBaseHelper(Context context){
        super(context, NAME_OF_DB, null, VERSION);
    }

    public static DataBaseHelper getInstanceOfDataBaseHelper(Context context){
        if(instanceOfDataBaseHelper == null){
            instanceOfDataBaseHelper = new DataBaseHelper(context);
        }
        return instanceOfDataBaseHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(usersTable.descriptionToCreateTable());
        db.execSQL(languagesTable.descriptionToCreateTable());
        db.execSQL(wordsTable.descriptionToCreateTable());
        db.execSQL(categoriesTable.descriptionToCreateTable());
        db.execSQL(learningTable.descriptionToCreateTable());
        db.execSQL(translateTable.descriptionToCreateTable());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion){
            db.execSQL("drop table if exists " + usersTable.nameOfTable());
            db.execSQL("drop table if exists " + languagesTable.nameOfTable());
            db.execSQL("drop table if exists " + wordsTable.nameOfTable());
            db.execSQL("drop table if exists " + categoriesTable.nameOfTable());
            db.execSQL("drop table if exists " + learningTable.nameOfTable());
            db.execSQL("drop table if exists " + translateTable.nameOfTable());
            onCreate(db);
        }
    }
}
