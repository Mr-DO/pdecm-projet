package com.univparisdiderot.learnnewslanguages.dao.table;

public class TableWords implements Table {

    public static final String name = "words";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_LANGUAGE = "language";
    public static final String COLUMN_WORD = "word";
    public static final String COLUMN_LOCALE_IMAGE = "localeImage";
    public static final String COLUMN_INTERNET_IMAGE = "internetImage";
    public static final String COLUMN_VOCAL = "vocal";

    private static final String descriptionTable = "CREATE TABLE " + name + " (" +
        COLUMN_ID + " integer PRIMARY KEY AUTOINCREMENT, " +
        COLUMN_LANGUAGE + " integer NOT NULL, " +
        COLUMN_WORD + " string UNIQUE NOT NULL, " +
        COLUMN_LOCALE_IMAGE + " string, " +
        COLUMN_INTERNET_IMAGE + " string, " +
        COLUMN_VOCAL + " string, " +
        "UNIQUE (" + COLUMN_LANGUAGE + ", " + COLUMN_WORD + ")" +
    ")";

    private static TableWords instance = null;

    public static TableWords getInstance(){
        if(instance == null){
            instance = new TableWords();
        }
        return instance;
    }

    @Override
    public String nameOfTable() {
        return name;
    }

    @Override
    public String descriptionToCreateTable() {
        return descriptionTable;
    }
}
