package com.univparisdiderot.learnnewslanguages.dao;

import android.content.ContentResolver;
import android.content.Context;

import com.univparisdiderot.learnnewslanguages.dao.table.Table;
import com.univparisdiderot.learnnewslanguages.dao.table.TableCategories;
import com.univparisdiderot.learnnewslanguages.dao.table.TableLanguages;
import com.univparisdiderot.learnnewslanguages.dao.table.TableLearning;
import com.univparisdiderot.learnnewslanguages.dao.table.TableTranslate;
import com.univparisdiderot.learnnewslanguages.dao.table.TableUsers;
import com.univparisdiderot.learnnewslanguages.dao.table.TableWords;

public class DataBase {

    public final static String AUTHORITY = "com.univparisdiderot.learnnewslanguages";

    public final static String NAME_OF_DB = "learn_new_languages";
    public final static Table usersTable = TableUsers.getInstance();
    public final static Table languagesTable = TableLanguages.getInstance();
    public final static Table wordsTable = TableWords.getInstance();
    public final static Table categoriesTable = TableCategories.getInstance();
    public final static Table learningTable = TableLearning.getInstance();
    public final static Table translateTable = TableTranslate.getInstance();

    private ContentResolver contentResolver;
    private static DataBase instance;

    private DataBase(Context context){
        this.contentResolver = context.getContentResolver();
    }

    public DataBase getInstance(Context context){
        if (instance == null){
            instance = new DataBase(context);
        }
        return instance;
    }


}
