package com.univparisdiderot.learnnewslanguages.dao.model;

import android.content.ContentValues;

public interface Model {

    ContentValues getContentValues();
}
