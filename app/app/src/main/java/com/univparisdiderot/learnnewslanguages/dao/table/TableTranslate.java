package com.univparisdiderot.learnnewslanguages.dao.table;

public class TableTranslate implements Table {

    public static final String name = "translate";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_WORD = "word";
    public static final String COLUMN_WORD_TRANSLATE = "wordTranslate";

    private static final String descriptionTable = "CREATE TABLE " + name + " (" +
        COLUMN_ID + " integer PRIMARY KEY AUTOINCREMENT, " +
        COLUMN_WORD + " integer NOT NULL, " +
        COLUMN_WORD_TRANSLATE + " integer NOT NULL, " +
        "FOREIGN KEY (" + COLUMN_WORD + ") REFERENCES " + TableWords.name + " (" + TableWords.COLUMN_ID + "), " +
        "FOREIGN KEY (" + COLUMN_WORD_TRANSLATE + ") REFERENCES " + TableWords.name + " (" + TableWords.COLUMN_ID + "), " +
        "UNIQUE (" + COLUMN_WORD + ", " + COLUMN_WORD_TRANSLATE + ")" +
    ")";


    private static TableTranslate instance = null;

    public static TableTranslate getInstance(){
        if(instance == null){
            instance = new TableTranslate();
        }
        return instance;
    }

    @Override
    public String nameOfTable() {
        return name;
    }

    @Override
    public String descriptionToCreateTable() {
        return descriptionTable;
    }
}
