package com.univparisdiderot.learnnewslanguages.dao.model;

import android.content.ContentValues;

import com.univparisdiderot.learnnewslanguages.dao.table.TableCategories;

public class Categorize implements Model {

    private ContentValues contentValues;
    private int id;
    private  String name;
    private  String description;

    public Categorize(int id, String name, String description){
        this.id = id;
        this.name = name;
        this.description = description;
    }

    // getters
    public int getId() {
        return id;
    }
    public String getName() {
        return this.name;
    }
    public String getDescription() {
        return this.description;
    }

    // setters
    public void setId(int id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public ContentValues getContentValues() {
        this.contentValues.put(TableCategories.COLUMN_NAME, this.name);
        this.contentValues.put(TableCategories.COLUMN_DESCRIPTION, this.description);
        return this.contentValues;
    }
}
