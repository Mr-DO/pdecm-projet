package com.univparisdiderot.learnnewslanguages.dao.table;

public interface Table {
    String nameOfTable();
    String descriptionToCreateTable();
}
