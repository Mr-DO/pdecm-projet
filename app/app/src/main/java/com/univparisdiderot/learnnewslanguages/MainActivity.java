package com.univparisdiderot.learnnewslanguages;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.univparisdiderot.learnnewslanguages.dao.table.Table;
import com.univparisdiderot.learnnewslanguages.dao.table.TableCategories;
import com.univparisdiderot.learnnewslanguages.dao.table.TableLanguages;
import com.univparisdiderot.learnnewslanguages.dao.table.TableLearning;
import com.univparisdiderot.learnnewslanguages.dao.table.TableTranslate;
import com.univparisdiderot.learnnewslanguages.dao.table.TableUsers;
import com.univparisdiderot.learnnewslanguages.dao.table.TableWords;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        List<Table> tables = new ArrayList<>();
        tables.add(TableUsers.getInstance());
        tables.add(TableLanguages.getInstance());
        tables.add(TableWords.getInstance());
        tables.add(TableCategories.getInstance());
        tables.add(TableLearning.getInstance());
        tables.add(TableTranslate.getInstance());

        for (Table table : tables ){
            System.out.println(table.descriptionToCreateTable());
        }
    }
}
