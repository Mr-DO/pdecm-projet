package com.univparisdiderot.learnnewslanguages.dao.model;

import android.content.ContentValues;

import com.univparisdiderot.learnnewslanguages.dao.table.TableUsers;

public class User implements Model{

    private ContentValues contentValues;
    private int id;
    private String username;
    private String password;
    private int baseLanguage;
    private int isAdmin;

    public User(int id, String username, String password, int baseLanguage, int isAdmin){
        this.id = id;
        this.username = username;
        this.password = password;
        this.baseLanguage = baseLanguage;
        this.isAdmin = isAdmin;
    }

    public int getId() {
        return this.id;
    }
    public String getUsername() {
        return this.username;
    }
    public String getPassword() {
        return this.password;
    }
    public int getBaseLanguage() {
        return this.baseLanguage;
    }
    public int getIsAdmin() {
        return this.isAdmin;
    }

    public void setId(int id) {
        this.id = id;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setBaseLanguage(int baseLanguage) {
        this.baseLanguage = baseLanguage;
    }
    public void setIsAdmin(int isAdmin) {
        this.isAdmin = isAdmin;
    }

    @Override
    public ContentValues getContentValues() {
        this.contentValues.put(TableUsers.COLUMN_USERNAME, this.username);
        this.contentValues.put(TableUsers.COLUMN_PASSWORD, this.password);
        this.contentValues.put(TableUsers.COLUMN_BASE_LANGUAGE, this.baseLanguage);
        this.contentValues.put(TableUsers.COLUMN_IS_ADMIN, this.isAdmin);
        return this.contentValues;
    }
}
