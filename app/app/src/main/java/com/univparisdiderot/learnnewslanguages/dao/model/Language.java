package com.univparisdiderot.learnnewslanguages.dao.model;

import android.content.ContentValues;

import com.univparisdiderot.learnnewslanguages.dao.table.TableLanguages;

public class Language implements Model {

    private ContentValues contentValues;

    private int id;
    private String name;

    public Language(int id, String name){
        this.id = id;
        this.name = name;
    }

    // getters
    public int getId() {
        return this.id;
    }
    public String getName() {
        return this.name;
    }

    // setters
    public void setId(int id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public ContentValues getContentValues() {
        this.contentValues.put(TableLanguages.COLUMN_NAME, this.name);
        return this.contentValues;
    }
}
