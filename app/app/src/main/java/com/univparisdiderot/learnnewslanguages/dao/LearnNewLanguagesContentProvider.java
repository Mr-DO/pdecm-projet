package com.univparisdiderot.learnnewslanguages.dao;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class LearnNewLanguagesContentProvider extends ContentProvider {

    private DataBaseHelper learnNewLanguagesDataBase;

    // Simple insert or search in data base
    private final static int INSERT_OR_GET_USERS = 1;
    private final static int INSERT_OR_GET_LANGUAGES = 2;
    private final static int INSERT_OR_GET_WORDS = 3;
    private final static int INSERT_OR_GET_LEARNING = 4;
    private final static int INSERT_OR_GET_CATEGORISES = 5;
    private final static int INSERT_OR_GET_TRANSLATE = 6;

    // Simple delete or update in data base
    private final static int DELETE_OR_UPDATE_USERS = 7;
    private final static int DELETE_OR_UPDATE_LANGUAGES = 8;
    private final static int DELETE_OR_UPDATE_WORDS = 9;
    private final static int DELETE_OR_UPDATE_LEARNING = 10;
    private final static int DELETE_OR_UPDATE_CATEGORISES = 11;
    private final static int DELETE_OR_UPDATE_TRANSLATE = 12;

    // Creates a UriMatcher object.
    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        uriMatcher.addURI(DataBase.AUTHORITY, DataBase.usersTable.nameOfTable(), INSERT_OR_GET_USERS);
        uriMatcher.addURI(DataBase.AUTHORITY, DataBase.languagesTable.nameOfTable(), INSERT_OR_GET_LANGUAGES);
        uriMatcher.addURI(DataBase.AUTHORITY, DataBase.wordsTable.nameOfTable(), INSERT_OR_GET_WORDS);
        uriMatcher.addURI(DataBase.AUTHORITY, DataBase.learningTable.nameOfTable(), INSERT_OR_GET_LEARNING);
        uriMatcher.addURI(DataBase.AUTHORITY, DataBase.categoriesTable.nameOfTable(), INSERT_OR_GET_CATEGORISES);
        uriMatcher.addURI(DataBase.AUTHORITY, DataBase.translateTable.nameOfTable(), INSERT_OR_GET_TRANSLATE);

        uriMatcher.addURI(DataBase.AUTHORITY, DataBase.usersTable.nameOfTable(), DELETE_OR_UPDATE_USERS);
        uriMatcher.addURI(DataBase.AUTHORITY, DataBase.languagesTable.nameOfTable(), DELETE_OR_UPDATE_LANGUAGES);
        uriMatcher.addURI(DataBase.AUTHORITY, DataBase.wordsTable.nameOfTable(), DELETE_OR_UPDATE_WORDS);
        uriMatcher.addURI(DataBase.AUTHORITY, DataBase.learningTable.nameOfTable(), DELETE_OR_UPDATE_LEARNING);
        uriMatcher.addURI(DataBase.AUTHORITY, DataBase.categoriesTable.nameOfTable(), DELETE_OR_UPDATE_CATEGORISES);
        uriMatcher.addURI(DataBase.AUTHORITY, DataBase.translateTable.nameOfTable(), DELETE_OR_UPDATE_TRANSLATE);
    }

    public LearnNewLanguagesContentProvider(){}

    @Override
    public boolean onCreate() {
        this.learnNewLanguagesDataBase = DataBaseHelper.getInstanceOfDataBaseHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        Cursor cursor;
        SQLiteDatabase db = this.learnNewLanguagesDataBase.getReadableDatabase();
        int code = uriMatcher.match(uri);
        switch (code){
            case INSERT_OR_GET_USERS:
                cursor = db.query(DataBase.usersTable.nameOfTable(), projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case INSERT_OR_GET_LANGUAGES:
                cursor = db.query(DataBase.languagesTable.nameOfTable(), projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case INSERT_OR_GET_WORDS:
                cursor = db.query(DataBase.wordsTable.nameOfTable(), projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case INSERT_OR_GET_LEARNING:
                cursor = db.query(DataBase.learningTable.nameOfTable(), projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case INSERT_OR_GET_CATEGORISES:
                cursor = db.query(DataBase.categoriesTable.nameOfTable(), projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case INSERT_OR_GET_TRANSLATE:
                cursor = db.query(DataBase.translateTable.nameOfTable(), projection, selection, selectionArgs, null, null, sortOrder);
                break;
            default:
                throw new UnsupportedOperationException("Not yet implemented, case in query search : " + uri.toString());
        }
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {

        SQLiteDatabase db = this.learnNewLanguagesDataBase.getWritableDatabase();
        int code = uriMatcher.match(uri);
        long success;
        switch (code){
            case INSERT_OR_GET_USERS:
                success =db.insertOrThrow(DataBase.usersTable.nameOfTable(), null, values);
                break;
            case INSERT_OR_GET_LANGUAGES:
                success =db.insertOrThrow(DataBase.languagesTable.nameOfTable(), null, values);
                break;
            case INSERT_OR_GET_WORDS:
                success =db.insertOrThrow(DataBase.wordsTable.nameOfTable(), null, values);
                break;
            case INSERT_OR_GET_LEARNING:
                success =db.insertOrThrow(DataBase.learningTable.nameOfTable(), null, values);
                break;
            case INSERT_OR_GET_CATEGORISES:
                success =db.insertOrThrow(DataBase.categoriesTable.nameOfTable(), null, values);
                break;
            case INSERT_OR_GET_TRANSLATE:
                success =db.insertOrThrow(DataBase.translateTable.nameOfTable(), null, values);
                break;
            default:
                throw new UnsupportedOperationException("Not yet implemented, case in insert search : " + uri.toString());
        }
        return new Uri.Builder().scheme("content").authority(DataBase.AUTHORITY).appendPath("Success").appendPath(String.valueOf(success)).build();
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase db = this.learnNewLanguagesDataBase.getWritableDatabase();
        int code = uriMatcher.match(uri);
        int success = -1;
        switch (code){
            case DELETE_OR_UPDATE_USERS:
                success = db.delete(DataBase.usersTable.nameOfTable(), selection, selectionArgs);
                break;
            case DELETE_OR_UPDATE_LANGUAGES:
                success =db.delete(DataBase.languagesTable.nameOfTable(), selection, selectionArgs);
                break;
            case DELETE_OR_UPDATE_WORDS:
                success =db.delete(DataBase.wordsTable.nameOfTable(), selection, selectionArgs);
                break;
            case DELETE_OR_UPDATE_LEARNING:
                success =db.delete(DataBase.learningTable.nameOfTable(), selection, selectionArgs);
                break;
            case DELETE_OR_UPDATE_CATEGORISES:
                success =db.delete(DataBase.categoriesTable.nameOfTable(), selection, selectionArgs);
                break;
            case DELETE_OR_UPDATE_TRANSLATE:
                success =db.delete(DataBase.translateTable.nameOfTable(), selection, selectionArgs);
                break;
        }
        return success;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase db = this.learnNewLanguagesDataBase.getWritableDatabase();
        int code = uriMatcher.match(uri);
        int success = -1;
        switch (code){
            case DELETE_OR_UPDATE_USERS:
                success = db.update(DataBase.usersTable.nameOfTable(), values, selection, selectionArgs);
                break;
            case DELETE_OR_UPDATE_LANGUAGES:
                success = db.update(DataBase.languagesTable.nameOfTable(), values, selection, selectionArgs);
                break;
            case DELETE_OR_UPDATE_WORDS:
                success = db.update(DataBase.wordsTable.nameOfTable(), values, selection, selectionArgs);
                break;
            case DELETE_OR_UPDATE_LEARNING:
                success = db.update(DataBase.learningTable.nameOfTable(), values, selection, selectionArgs);
                break;
            case DELETE_OR_UPDATE_CATEGORISES:
                success = db.update(DataBase.categoriesTable.nameOfTable(), values, selection, selectionArgs);
                break;
            case DELETE_OR_UPDATE_TRANSLATE:
                success = db.update(DataBase.translateTable.nameOfTable(), values, selection, selectionArgs);
                break;
        }
        return success;
    }
}
