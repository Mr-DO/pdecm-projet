package com.univparisdiderot.learnnewslanguages.dao.table;

public class TableUsers implements Table {

    public static final String name = "users";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_USERNAME = "username";
    public static final String COLUMN_PASSWORD = "password";
    public static final String COLUMN_BASE_LANGUAGE = "baseLanguage";
    public static final String COLUMN_IS_ADMIN = "isAdmin";

    private static final String descriptionTable = "CREATE TABLE " + name + " (" +
        COLUMN_ID + " integer PRIMARY KEY AUTOINCREMENT, " +
        COLUMN_USERNAME + " string UNIQUE NOT NULL, " +
        COLUMN_PASSWORD + " string NOT NULL, " +
        COLUMN_BASE_LANGUAGE + " string NOT NULL, " +
        COLUMN_IS_ADMIN + " integer DEFAULT 0, " +
        "FOREIGN KEY (" + COLUMN_BASE_LANGUAGE + ") REFERENCES " + TableLanguages.name + " (" + TableLanguages.COLUMN_ID + ")" +
     ")";

    private static TableUsers instance = null;

    public static TableUsers getInstance(){
        if(instance == null){
            instance = new TableUsers();
        }
        return instance;
    }

    @Override
    public String nameOfTable() {
        return name;
    }

    @Override
    public String descriptionToCreateTable() {
        return descriptionTable;
    }
}
